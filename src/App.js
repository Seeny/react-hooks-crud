import React, {useState} from 'react';
import UserTable from './components/UserTable';
import AddUserForm from './components/AddUserForm'
import EditUserForm from './components/EditUserForm'


import { v4 as uuidv4 } from 'uuid';


function App() {

  const usersData = [
    { id: uuidv4(), name: 'Tania', username: 'floppydiskette' },
    { id: uuidv4(), name: 'Craig', username: 'siliconeidolon' },
    { id: uuidv4(), name: 'Ben', username: 'benisphere' },
  ]

  const [Users, setUsers] = useState(usersData)

  //add users

  const addUser = (user) => {
    user.id = uuidv4()
    setUsers([
      ...Users,
      user
    ])
  }

  // Eliminar User

  const deleteUser = (id) =>{
    setUsers(Users.filter(user => user.id !== id))
  }

  // Editar Usuario
  const [Editing, setEditing] = useState(false);
  const [currentUser, setcurrentUser] = useState({
    id: null, 
    name: '', 
    username:''
  });

  const editRow = (user) => {
    setEditing(true);
    setcurrentUser({
      id: user.id,
      name: user.name,
      username: user.username
    })
  }

  const updateUser = (id, updateUser) => {
    setEditing(false);
    setUsers(Users.map(user => (user.id === id ? updateUser : user)))
  }

  return (
    <div className="container">
      <h1>CRUD App with Hooks</h1>
      <div className="flex-row">
        <div className="flex-large">
          {
            Editing ? (
              <div>
                <h2>Edit user</h2>
                  <EditUserForm
                    currentUser={currentUser}
                    updateUser={updateUser}
                  />
                </div>
            ) : (
              <div>
                  <h2>Add user</h2>
                  <AddUserForm addUser={addUser}/>
              </div>
            )
          }
          
          
        </div>
        <div className="flex-large">
          <h2>View users</h2>
          <UserTable 
            users={Users} 
            deleteUser={deleteUser} 
            editRow={editRow}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
